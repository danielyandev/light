<?php

namespace App\Console\Commands;

use System\core\base\console\Command;
use System\Core\Base\Console\Console;

class CacheCommand extends Command
{
    /**
     * Clears cache
     */
    public function clear()
    {
        $command = "rm -r app/Storage/cache/* --force";

        $this->execute($command);
        Console::log('success', 'Cache cleared successfully');
    }
}