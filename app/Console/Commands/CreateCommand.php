<?php

namespace App\Console\Commands;


use System\core\base\console\Command;
use System\Core\Base\Console\Console;

class CreateCommand extends Command
{
    /**
     * Creates file by the path with code inside
     *
     * @param $path
     * @param $code
     */
    public function createFile($path, $code)
    {
        $file = fopen($path, 'w');
        fwrite($file, $code);
        fclose($file);
    }
    /**
     * Creates controller
     *
     * @param $name
     * @param array $options
     * @return bool
     */
    public function controller($name, $options = [])
    {
        if (!$name){
            Console::log("error", "Controller name is missing");
            Console::log("warning", "Type make:controller <ControllerName>");
            return false;
        }

        $controller_path = 'app/Controllers/'. $name . '.php';
        $controller_namespace = 'App\Controllers';

        if (!is_file($controller_path)){
            $code =
            "<?php\n".
            "\n".
            "namespace $controller_namespace;\n".
            "\n".
            "class $name extends Controller\n".
            "{\n".
                "\t//\n" .
            "}";

            $this->createFile($controller_path, $code);

            Console::log('success', 'Controller '. $name. ' created successfully');
            return true;
        }

        Console::log('error', 'Controller with name ' . $name . ' already exists');
        return false;
    }

    /**
     * Creates model
     *
     * @param $name
     * @param array $options
     * @return bool
     */
    public function model($name, $options = [])
    {
        if (!$name){
            Console::log("error", "Model name is missing");
            Console::log("warning", "Type make:model <ModelName>");
            return false;
        }

        $model_path = 'app/Models/'. $name . '.php';
        $model_namespace = 'App\Models';

        if (!is_file($model_path)){
            $code =
                "<?php\n".
                "\n".
                "namespace $model_namespace;\n".
                "\n".
                "use Illuminate\Database\Eloquent\Model;\n".
                "\n".
                "class $name extends Model\n".
                "{\n".
                "\t//\n" .
                "}";

            $this->createFile($model_path, $code);

            Console::log('success', 'Model '. $name. ' created successfully');
            return true;
        }

        Console::log('error', 'Model with name ' . $name . ' already exists');
        return false;
    }

    public function migration($name, $options = [])
    {
        if (!$name){
            Console::log("error", "Migration name is missing");
            Console::log("warning", "Type make:migration <MigrationName>");
            return false;
        }

        $migration_path = 'app/Migrations/'. $name . '.php';
        $migration_namespace = 'App\Migrations';

        if (!is_file($migration_path)){
            $code =
                "<?php\n".
                "\n".
                "namespace $migration_namespace;\n".
                "\n".
                "use Illuminate\Database\Eloquent\Model;\n".
                "\n".
                "class $name extends Model\n".
                "{\n".
                "\t//\n" .
                "}";

            $this->createFile($migration_path, $code);

            Console::log('success', 'Migration '. $name. ' created successfully');
            return true;
        }

        Console::log('error', 'Migration with name ' . $name . ' already exists');
        return false;
    }
}