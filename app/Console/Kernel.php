<?php

namespace App\Console;

use System\Core\Base\Console\Console;
use System\Core\Base\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->setRegisteredCommands();
        parent::__construct();
    }

    /**
     * Register console commands here with description
     *
     * Note: You need to register only command names without arguments
     *
     * E.g. SomeCommandClass:method,
     * if there is no method, method run() will be called
     */
    public function setRegisteredCommands()
    {
        $this->registeredCommands = [
            // App\Console\Commands\CreateCommand
            Console::command('create:controller', 'Creates controller'),
            Console::command('create:model', 'Creates model'),
            Console::command('create:migration', 'Creates migration'),

            // App\Console\Commands\CacheCommand
            Console::command('cache:clear', 'Clears cache')
        ];
    }
}