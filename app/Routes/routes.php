<?php

use System\Core\Base\Routes\Route;
use System\Core\Base\Views\View;

return [

    Route::make('/', function (){
        View::make('index');
    }),

];