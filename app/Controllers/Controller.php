<?php

namespace App\Controllers;

use System\Core\Base\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    // Extend all your controllers from this controller
}