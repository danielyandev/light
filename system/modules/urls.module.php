<?php

/**
 * @param $url
 * @return string
 */
function convertUrl($url)
{
    $arr[] = '#^';
    $arr[] = preg_replace_callback('#\{[A-z0-9]+\}#', function ($match){
        return '(.+?)';
    }, $url);
    $arr[] = '/*$#i';
    if ($url[0] == '/'){
        return join('', $arr);
    }
    return '/'.join('', $arr);
}

/**
 * @param $path
 * @return string
 */
function asset($path)
{
    return "../app/assets/$path";
}