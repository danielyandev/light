<?php

namespace System\Core\Base\Routes;


class Route
{
    /**
     * @param $route
     * @param $method
     * @return array
     */
    public static function make($route, $method)
    {
        return [$route => $method];
    }

}