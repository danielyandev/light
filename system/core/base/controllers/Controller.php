<?php

namespace System\Core\Base\Controllers;

use Twig_Environment;
use Twig_Loader_Filesystem;

class Controller
{
    /**
     * Params to pass to view
     *
     * @var array $params
     */
    public $params = [];

    /**
     * View to render
     *
     * @var mixed $view
     */
    public $view;

    /**
     * @param $view
     * @param array $params
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($view, $params = [])
    {
        $view .= '.html';

        $this->view = $view;
        $this->params = $params;

        $view_path = APP_DIR .'/Views';
        $cache_path = APP_DIR .'/Storage/cache';

        $loader = new Twig_Loader_Filesystem($view_path);
        $twig = new Twig_Environment($loader, [
            'cache' => $cache_path
        ]);

        echo $twig->render($this->view, $this->params);
    }

}