<?php

namespace System\Core\Base\Console;

class Kernel
{
    /**
     * @var array $registeredCommands
     */
    protected $registeredCommands = [];

    /**
     * @var array $availableCommands
     */
    protected $availableCommands = [];

    public function __construct()
    {
        foreach ($this->registeredCommands as $commands){
            foreach ($commands as $command => $description) {
                $this->availableCommands[$command] = $description;
            }
        }
    }

    /**
     * Handles command
     *
     * @param $args
     * @return bool
     */
    public function handle($args)
    {
        $command = [];

        foreach ($args as $key => $arg) {
            // remove $args[0] - it is name of file (light)
            if ($key){
                $command[] = $arg;
            }
        }

        if ($command){
             return $this->run($command);
        }

        foreach ($this->availableCommands as $command => $description) {
            Console::log('success', $command . "\t\t\t" . $description);
        }

        return false;
    }

    /**
     * Runs command
     *
     * @param array $command
     * @return bool
     */
    public function run(Array $command)
    {
        $mainCommand = $command[0];
        array_shift($command);

        $args = '';
        //TODO fix options
        $options = [];

        foreach ($command as $key => $value) {
            if ($value[0] == '-'){
                $options[] = $value;
            }else{
                if (!$args){
                    $args .= $value;
                }else{
                    $args .= ','. $value;
                }
            }
        }

        if(array_key_exists($mainCommand, $this->availableCommands)){
            return $this->runCommand($mainCommand, $args, $options);
        }
        
        return false;
    }

    /**
     * Returns available commands with descriptions
     *
     * @return array
     */
    public function getAvailableCommands()
    {
        return $this->availableCommands;
    }

    /**
     * Parses commands described in file into array
     *
     * @param $all_commands
     * @return array
     */
    public function parseCommands($all_commands)
    {
        $parsed = [];
        foreach ($all_commands as $key => $commands){
            foreach ($commands as $command => $description) {
                $parsed[$command] = $description;
            }
        }

        return $parsed;
    }

    /**
     * @param $command
     * @param $args
     * @param $options
     * @return bool
     */
    public function runCommand($command, $args, $options)
    {
        $command = explode(':', $command);

        $controller = ucfirst($command[0]);
        $method = isset($command[1]) ? $command[1] : 'run';

        $controller = '\App\Console\Commands\\'.$controller.'Command';

        $controller = new $controller();
        $controller->$method($args, $options);

        return true;
    }
}