<?php

namespace System\Core\Base\Console;


class Command
{
    /**
     * Executes console command
     *
     * @param $command
     * @return string
     */
    public function execute($command)
    {
        return exec($command);
    }
}