<?php

namespace System\Core\Base\Console;


class Console
{
    /**
     * Colors for console output
     *
     * @var array $outputTypes
     */
    protected static $outputTypes = [
        'info' => '1;34m',
        'error' => '0;31m',
        'success' => '0;32m',
        'warning' => '1;33m',
    ];

    /**
     * @var string $outputTypeStart
     */
    protected static $outputTypeStart = "\e[";

    /**
     * @var string $outputTypeEnd
     */
    protected static $outputTypeEnd = "\e[0m\n";

    public static function command($command, $description)
    {
        return [$command => $description];
    }

    /**
     * Logs into terminal
     *
     * @param $type
     * @param $string
     */
    public static function log($type, $string)
    {
        $color = self::$outputTypes[$type];
        echo self::$outputTypeStart. $color . $string . self::$outputTypeEnd;
    }
}