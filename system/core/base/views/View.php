<?php

namespace System\Core\Base\Views;

use System\Core\Base\Views\Twig\LightTwigEnvironment;
use Twig_Environment;
use Twig_Loader_Filesystem;

class View
{
    /**
     * @param $view
     * @param array $params
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function make($view, $params = [])
    {
        $view .= '.html';
        $view_path = APP_DIR .'/Views';
        $cache_path = APP_DIR .'/Storage/cache';

        $loader = new Twig_Loader_Filesystem($view_path);
//        $twig = new Twig_Environment($loader, [
//            'cache' => $cache_path
//        ]);
        $twig = new LightTwigEnvironment($loader, [
            'cache' => $cache_path
        ]);

        echo $twig->render($view, $params);
    }

}