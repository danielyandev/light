<?php

namespace System\Core\Base\Sessions;


class Session
{
    /**
     * Key to get value from flash
     *
     * @var string $key
     */
    private static $key = 'flash';

    /**
     * Session instance
     *
     * @var Session $session
     */
    public static $session;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        $this->start();
    }

    /**
     * Return session instance
     *
     * @return mixed
     */
    public function start()
    {
        if (self::$session)
        {
            return self::$session;
        }

        session_start();
        self::$session =  $_SESSION;
    }

    /**
     * Set flash value into session
     *
     * @param $key
     * @param array $value
     */
    public static function flash($key, $value=[] )
    {
        $_SESSION[self::$key][$key] = $value;
    }

    /**
     * Get flashed value from session
     *
     * @param $key
     * @return bool
     */
    public static function getFlash($key)
    {
        if( isset( $_SESSION[self::$key][$key] ) )
        {
            $value = $_SESSION[self::$key][$key];
            unset( $_SESSION[self::$key][$key] );
            return $value;
        }
        return false;
    }

    /**
     * Put value to session
     *
     * @param $key
     * @param array $value
     */
    public static function put($key, $value=[])
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Get key value from session
     *
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return $_SESSION[$key];
    }

    /**
     * Delete key from session
     *
     * @param $key
     */
    public static function forget($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * Returns all values from session
     *
     * @return mixed
     */
    public static function all()
    {
        return self::$session;
    }
}