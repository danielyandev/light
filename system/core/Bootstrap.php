<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use System\Core\Base\Sessions\Session;

class Bootstrap
{

    /**
     * Current application
     *
     * @var $app
     */
    public $app;

    /**
     * Uri caught by router
     *
     * @var string $uri
     */
    public $uri;

    /**
     * Session instance
     *
     * @var Session $session
     */
    public $session;

    /**
     * Bootstrap constructor.
     */
    public function __construct()
    {
        $this->session = new Session();
        $this->uri = urldecode(preg_replace('/\?.*/iu', '', $_SERVER['REQUEST_URI']));
        $this->loadORM();
        $this->setController();
    }

    /**
     * Creates database connection to use models
     */
    public function loadORM()
    {
        $capsule = new Capsule;
        $config = require APP_DIR.'/Config/db.php';
        $capsule->addConnection($config);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    /**
     * Runs controller method caught by router
     */
    public function setController()
    {
        $routes = require APP_DIR.'/Routes/routes.php';
        foreach ($routes as $mainroute) {
            foreach ($mainroute as $route => $method) {
                $args = [];
                if(preg_match(convertUrl($route), $this->uri, $args))
                {
                    unset($args[0]);
                    $args = implode(',', $args);

                    $this->app = true;
                    if(is_string($method))
                    {
                        $method = explode('@', $method);
                        $controller = $method[0];
                        $method = $method[1];
                        $controller = '\App\Controllers\\'.$controller;
                        
                        $controller = new $controller();
                        $controller->$method($args);
                        break(2);
                    }elseif (is_callable($method)){
                        $method($args);
                    }
                }
            }
        }
    }
}