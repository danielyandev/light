<?php

/**
 * Define base variables
 */
define('BASE_DIR', getcwd());
define('APP_DIR', BASE_DIR .'/app');
define('SYSTEM_DIR', BASE_DIR .'/system');

/**
 * Connect composer
 */
require BASE_DIR.'/vendor/autoload.php';

/**
 * Connect modules
 */
$modules = glob(SYSTEM_DIR.'/modules/*.module.php');
foreach ($modules as $module){
    require $module;
}

/**
 * Connect Bootstrap class
 */
require_once __DIR__ .'/core/Bootstrap.php';