# Light
##Light Framework for light projects

##Get Started

##### Clone from GitHub: git clone https://github.com/danielyandev/light.git

##Usage
<pre>
Work with database easily:
                            
                            <span>$user = User::where('name', $name)->get();</span>
                            
                            
Pass the data to your views using Twig:

                            <span>
                            (View path: app/Views/welcome.html)
                            
                            View::make('welcome', [
                                   'user' => $user
                            ]);
                            
                            
Easy Sessions:
                            
                            Session::flash('user', $user);
                            Session::getFlash('user);
                            </span>
                            
</pre>
